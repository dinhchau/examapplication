package com.vnpt.examapplication.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.vnpt.examapplication.R;
import com.vnpt.examapplication.constants.ConstText;
import com.vnpt.examapplication.model.request.LoginRequest;
import com.vnpt.examapplication.model.response.LoginResponse;
import com.vnpt.examapplication.network.ApiManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.tvLoginTitle)
    TextView tvLoginTitle;

    @NotEmpty
    @BindView(R.id.edUsername)
    EditText edUsername;

    @Password(min = 8)
    @BindView(R.id.edPassword)
    EditText edPassword;
    @BindView(R.id.ibShowPass)
    ImageButton ibShowPass;
    @BindView(R.id.btLogin)
    Button btLogin;
    @BindView(R.id.tvForgetPassword)
    TextView tvForgetPassword;
    @BindView(R.id.btnFingerPrint)
    Button btnFingerPrint;
    @BindView(R.id.cbRemember)
    CheckBox cbRemember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loadDataSaved();
    }

    @OnClick({R.id.ibShowPass, R.id.btLogin, R.id.tvForgetPassword, R.id.btnFingerPrint})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibShowPass:
                break;
            case R.id.btLogin:
                login();
                break;
            case R.id.tvForgetPassword:
                break;
            case R.id.btnFingerPrint:
                break;
        }
    }

    public void saveSetting(String token) {
        SharedPreferences setting = getSharedPreferences(ConstText.APP_NAME, MODE_PRIVATE);

        SharedPreferences.Editor editor = setting.edit();
        editor.putString(ConstText.USER_TOKEN, token);
        if(cbRemember.isChecked()){
            editor.putString(ConstText.USERNAME, edUsername.getText().toString());
            editor.putString(ConstText.PASSWORD, edPassword.getText().toString());
            editor.putBoolean(ConstText.REMEMBER_PW, true);
        }
        editor.commit();
    }

    public void loadDataSaved() {
        SharedPreferences setting = getSharedPreferences(ConstText.APP_NAME, MODE_PRIVATE);
        String username = setting.getString(ConstText.USERNAME, null);
        String password = setting.getString(ConstText.PASSWORD, null);
//        Boolean is_remember = setting.getBoolean(ConstText.REMEMBER_PW, false);
        edUsername.setText(username);
        edPassword.setText(password);
//        cbRemember.setChecked(is_remember);
    }

    public void checkToken() {
        SharedPreferences setting = getSharedPreferences(ConstText.APP_NAME, MODE_PRIVATE);
        String token = setting.getString(ConstText.USER_TOKEN, null);

    }

    public void goMain() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("Username", edUsername.getText().toString());
        startActivity(intent);
    }

    private void login() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiManager.SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiManager service = retrofit.create(ApiManager.class);

        service.login(new LoginRequest(edUsername.getText().toString(), edPassword.getText().toString(), ConstText.MAMAY, ConstText.HEDIEUHANH, ConstText.FIREBASE_TOKEN)).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.body() == null) {
                    Toast.makeText(LoginActivity.this, "Không bắt được dữ liệu!!!", Toast.LENGTH_LONG);
                } else {
                    LoginResponse model = response.body();
                    if (model.getErrorCode() == 0) {
                        saveSetting(model.getToken());
                        goMain();
                    } else {
                        Toast.makeText(LoginActivity.this, model.getErrorDesc(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("TAG", "Error: " + t);
            }
        });
    }
}